import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;


public class Panneau extends JPanel {

	
	private int posX;
	private int posY;
	

	public Panneau() {
		this.posX = 1400;
		this.posY = 600;
	}


	public int getPosX() {
		return posX;
	}


	public void setPosX(int posX) {
		this.posX = posX;
	}


	public int getPosY() {
		return posY;
	}


	public void setPosY(int posY) {
		this.posY = posY;
	}


	public void paintComponent(Graphics g){
		try {
			Image stickman = ImageIO.read(new File("stickman.png"));
			Image fond = ImageIO.read(new File("fond.png"));


			g.drawImage(fond, 0, 0, this.getWidth(), this.getHeight(), this);
			g.drawImage(stickman, posX, posY, this);

		} catch (IOException e) {
			e.printStackTrace();
		}                
	}               
}